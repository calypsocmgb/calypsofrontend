import { GlobeComponent } from "./homepage/body-container/globe/globe.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./homepage/header-container/header/header.component";
import { FooterComponent } from "./homepage/footer/footer.component";
import { FormulaireComponent } from "./homepage/header-container/formulaire/formulaire.component";
import { HeaderContainerComponent } from "./homepage/header-container/header-container.component";
import { NavbarComponent } from "./homepage/navbar/navbar.component";
import { BodyComponent } from "./homepage/body-container/body.component";
import { RechercheVoyageComponent } from "./recherche-voyage/recherche-voyage.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { CreervoyageComponent } from "./creervoyage/creervoyage.component";
import { PresentationComponent } from "./presentation/presentation.component";
import { ResultatTotalVoyagesComponent } from './resultat-total-voyages/resultat-total-voyages.component';
import { DetailVoyageComponent } from './detail-voyage/detail-voyage.component';
import { ConnexionComponent } from './connexion/connexion.component';


const APP_ROUTE: Route[] = [
  { path: "", component: HomepageComponent },
  { path: "recherche", component: RechercheVoyageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FormulaireComponent,
    HeaderContainerComponent,
    NavbarComponent,
    BodyComponent,
    GlobeComponent,
    FormulaireComponent,
    ConnexionComponent,
    RechercheVoyageComponent,
    HomepageComponent,
    CreervoyageComponent,
    PresentationComponent,
    ResultatTotalVoyagesComponent,
    DetailVoyageComponent,
    ConnexionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(APP_ROUTE),
    HttpClientModule
  ],
  providers: [
    /*OktaAuthWrapper*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
