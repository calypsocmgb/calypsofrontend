export class credential {
  
  public identifiant: string;
  public password: string;

  constructor(identifiant: string, motdepasse: string) {
    this.identifiant = identifiant;
    this.password = motdepasse;
  }
}
