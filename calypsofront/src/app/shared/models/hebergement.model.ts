import { Voyage } from "./voyage.model";
export class Hebergement {
  public id: number;
  public pays: string;
  public region: string;
  public adresse: string;
  public nom: string;
  public type: string;
  public Voyage: Voyage;

  get _nom():String{
    return this.nom;
  }

  constructor(
    id: number,
    pays: string,
    region: string,
    adresse: string,
    nom: string,
    type: string,
    Voyage: Voyage
  ) {
    this.id = id;
    this.pays = pays;
    this.region = region;
    this.adresse = adresse;
    this.nom = nom;
    this.type = type;
    this.Voyage = Voyage;
  }
}
