import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { credential } from '../shared/models/credential.model';
import { Utilisateur } from '../shared/models/utilisateur.model';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { RectangleOutlineGeometry } from 'cesium';

const httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
    })
  };
const newLocal = "application/json";


@Injectable()
export class connexionservice {

    constructor(
    private http: HttpClient) {
    }

    userConnexion(credential : credential)
    {
      return this.http.post<Utilisateur>("https://localhost:44335/api/users/AuthPass",credential)
      .pipe(map((data : Utilisateur)=> {return data;}), catchError( error => {return throwError('Something went wrong')}));
    }


}