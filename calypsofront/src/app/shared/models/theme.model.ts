export class Theme {
  public id: number;
  public theme: string;

  constructor(id: number, theme: string) {
    this.id = id;
    this.theme = theme;
  }
}
