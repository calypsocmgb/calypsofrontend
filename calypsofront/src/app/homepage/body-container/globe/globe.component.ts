import { Component, OnInit, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Voyage } from 'src/app/shared/models/voyage.model';

@Component({
  selector: "app-globe",
  templateUrl: "./globe.component.html",
  styleUrls: ["./globe.component.css"]
})
@Injectable()
export class GlobeComponent implements OnInit {
  VoyageAustralie: Array<Voyage> = [];
  VoyageRussie: Array<Voyage> = [];
  VoyageCongo: Array<Voyage> = [];
  VoyageHongrie: Array<Voyage> = [];
  VoyageGroenland: Array<Voyage> = [];
  VoyageEU: Array<Voyage> = [];
  constructor(private http: HttpClient) {}
  pathDetail: string = "";


  ngOnInit() {
    this.pathDetail = "detail";
    let authen = new HttpHeaders();
    let authen2 = authen.append(
      "Authorization",
      "Basic YXBwLW5hbWU6Y2FseXBzbw=="
    );
    
    let authen3 = authen2.append("Access-Control-Allow-Origin", "*");
   
    this.getVoyagesAustralie(authen3);
    this.getVoyagesRussie(authen3);
    this.getVoyagesCongo(authen3);
    this.getVoyagesHongrie(authen3);
    this.getVoyagesGroenland(authen3);
    this.getVoyagesEU(authen3);
  }

  getVoyagesAustralie(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Australie", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageAustralie = result;
      });
  }

  getVoyagesRussie(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Russie", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageRussie = result;
      });
  }

  getVoyagesCongo(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Congo", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageCongo = result;
      });
  }

  getVoyagesHongrie(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Hongrie", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageHongrie = result;
      });
  }

  getVoyagesGroenland(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Groenland", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageGroenland = result;
      });
  }

  getVoyagesEU(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/RechercheVoyagesPays/Etatsunis", {
        headers: authen
      })
      .subscribe(result => {
        this.VoyageEU = result;
      });
  }




}
