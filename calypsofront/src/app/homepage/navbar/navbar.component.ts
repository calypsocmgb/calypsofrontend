import { Component, OnInit } from "@angular/core";
import { Role } from "src/app/shared/models/role.model";
import { Utilisateur } from "src/app/shared/models/utilisateur.model";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  user: string = "client";
  item1: string = "Accueil";
  item2: string = "Présentation";
  item3: string = "Rechercher un voyage";
  item4: string = "Voyages";
  sousItem1: string = "Randonnée";
  sousItem2: string = "Balnéaire";
  pathItem1: string = "";
  pathItem2: string = "presentation";
  pathItem3: string = "recherche";
  pathSousItem1: string = "";
  pathSousItem2: string = "";
  constructor() {}

  ngOnInit() {
    /*let client = new Role(1, "client", null);
    let salarié = new Role(2, "salarié", null);
    let user = new Utilisateur(
      1,
      "Morillon",
      "Cyrille",
      "293 Avenue de Dunkerque",
      "29",
      "teclise@hotmail.fr",
      "0699565654",
      null,
      null,
      null
    );
    if (user.Role == client) {
      this.item1 = "Accueil";
      this.item2 = "Présentation";
      this.item3 = "Rechercher un voyage";
      this.item4 = "Voyages";
      this.sousItem1 = "Randonnée";
      this.sousItem2 = "Balnéaire";
      this.pathItem1 = "/";
      this.pathItem2 = "presentation";
      this.pathItem3 = "recherche";
      this.pathSousItem1 = "randonnée";
      this.pathSousItem2 = "balneaire";
    } else if (user.Role == salarié) {
      this.item1 = "Créer un voyage";
      this.item2 = "Supprimer un voyage";
      this.item3 = "Dossiers clients";
    }*/
  }
}
