import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import {Router} from '@angular/router';
import { HttpHeaders, HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Utilisateur } from '../shared/models/utilisateur.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { credential } from '../shared/models/credential.model';
import { Observable } from 'rxjs';
import { connexionservice } from './connexion.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-connexion',
  providers: [connexionservice],
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})

@Injectable()
export class ConnexionComponent implements OnInit {


  constructor(private connexions : connexionservice,
    private router : Router) {
    }

  ngOnInit(){this.submitted = false;}

  modalService: BsModalService;
  submitted : Boolean;
  model = new credential("login","password");
  utilisateurFinal : Utilisateur = new Utilisateur(1,"empty","empty","empty",105,"empty","empty","empty","empty","empty","empty","empty",{"identifiant":"empty","password":"empty"});
    
  onSubmit() {
          this.submitted = true;
          const userCO = new credential(this.model.identifiant,this.model.password);
          console.log(this.model)
          this.retourConnexion(userCO);
  }

  retourConnexion(credential : credential) : void
  {
    this.connexions.userConnexion(credential)
    .subscribe((data : any)=>{ console.log(data),this.utilisateurFinal = data,console.log(this.utilisateurFinal)});
    if(this.utilisateurFinal.nom!=="empty"){
    window.alert("Félécitation vous êtes dorénavant connecté !");
    this.creationlocalstorage(this.utilisateurFinal);
    this.goAc();
   }
  }

  goAc()
  {
    this.router.navigate(['/']);
  }

  creationlocalstorage(user : Utilisateur)
  {
    let myobj = JSON.stringify(user);
    localStorage.setItem("user",myobj);
  }

  

}
