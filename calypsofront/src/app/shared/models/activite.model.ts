import { Voyage } from "./voyage.model";
export class Activite {
  public id: number;
  public nom: string;
  public adresse: string;
  public pays: string;
  public region: string;
  public prix: number;
  public Voyage: Voyage;

  constructor(
    id: number,
    nom: string,
    adresse: string,
    pays: string,
    region: string,
    prix: number,
    Voyage: Voyage
  ) {
    this.id = id;
    this.nom = nom;
    this.adresse = adresse;
    this.pays = pays;
    this.region = region;
    this.prix = prix;
    this.Voyage = Voyage;
  }
}
