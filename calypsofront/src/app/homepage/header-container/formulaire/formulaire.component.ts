import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {

  constructor(
    private router : Router
  ) { }
   
  usertrue =false;
  userCO =true;
  ngOnInit() {
    if(localStorage.length>0)
    {
      this.usertrue = true;
      this.userCO = false;
    }

  }

}
