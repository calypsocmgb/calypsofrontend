import { DetailVoyageComponent } from "./detail-voyage/detail-voyage.component";
import { RechercheVoyageComponent } from "./recherche-voyage/recherche-voyage.component";
import { PresentationComponent } from "./presentation/presentation.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule, Router } from "@angular/router";
import { ResultatTotalVoyagesComponent } from "./resultat-total-voyages/resultat-total-voyages.component";
import { ConnexionComponent } from './connexion/connexion.component';
import { AppComponent } from './app.component';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from 'selenium-webdriver/http';

const routes: Routes = [
  {
    path: "presentation",
    component: PresentationComponent
  },
  {
    path: "recherche",
    component: RechercheVoyageComponent,
    children: [{ path: "resultat", component: ResultatTotalVoyagesComponent }]
  },
  { path: "detail", component: DetailVoyageComponent },
  { path:"connexion", component: ConnexionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
