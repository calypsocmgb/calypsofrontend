import { Utilisateur } from "./utilisateur.model";
export class Role {
  public id: number;
  public role: string;
  public Utilisateur: Array<Utilisateur>;

  constructor(id: number, role: string, Utilisateur: Array<Utilisateur>) {
    this.id = id;
    this.role = role;
    this.Utilisateur = Utilisateur;
  }
}
