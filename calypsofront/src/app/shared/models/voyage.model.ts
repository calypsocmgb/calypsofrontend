
import { Utilisateur } from "./utilisateur.model";
import { TransportInterne } from "./transportInterne.model";
import { Activite } from "./activite.model";
import { Hebergement } from "./hebergement.model";
import { Theme } from "./theme.model";
import { Pension } from "./pension.model";
import { ResultatTotalVoyagesComponent } from 'src/app/resultat-total-voyages/resultat-total-voyages.component';
import { Subject } from 'rxjs';
export class Voyage {
  public voyageAffiche = new Subject<any[]>();
  public listVoyages = ResultatTotalVoyagesComponent.getVoyages();
  public id: number;
  public nom: string;
  public dateDebut: string;
  public dateFin: string;
  public nombreVoyageur: number;
  public itinerance: boolean;
  public prix: number;
  public detail: string;
  public pays: string;
  public Utilisateur: Utilisateur;
  public Pension: Pension;
  public Theme: Theme;
  public Hebergement: Array<Hebergement>;
  public Activite: Array<Activite>;
  public TransportInterne: Array<TransportInterne>;
  constructor(
    id: number,
    nom: string,
    dateDebut: string,
    dateFin: string,
    nombreVoyageur: number,
    itinerance: boolean,
    prix: number,
    detail: string,
    pays: string,
    Utilisateur: Utilisateur,
    Pension: Pension,
    Theme: Theme,
    Hebergement: Array<Hebergement>,
    Activite: Array<Activite>,
    TransportInterne: Array<TransportInterne>
  ) {
    this.id = id;
    this.nom = nom;
    this.dateDebut = dateDebut;
    this.dateFin = dateFin;
    this.nombreVoyageur = nombreVoyageur;
    this.itinerance = itinerance;
    this.prix = prix;
    this.detail = detail;
    this.pays = pays;
    this.Utilisateur = Utilisateur;
    this.Pension = Pension;
    this.Theme = Theme;
    this.Hebergement = Hebergement;
    this.Activite = Activite;
    this.TransportInterne = TransportInterne;
  }

  

    getListVoyages(pays: string, theme:string) {

    return this.listVoyages.filter(
      (voyage) => {
        if(voyage.pays == pays && voyage.Theme.theme == theme) {
          return true
        }
      }
    )
    }

  /*getListVoyages(pays: string, prix: string) {

    return this.listVoyages.filter(
      (voyage) => {
        if(voyage.pays == pays && voyage.prix == Number.parseFloat(prix)) {
          return true
        }
      }
    )*/

    //Autre méthodologie plus rapide
    /*return this.listVoyages.filter(
      voyage => (voyage.Theme.theme == theme && voyage.prix == Number.parseFloat(prix))
    )*/

  
}
