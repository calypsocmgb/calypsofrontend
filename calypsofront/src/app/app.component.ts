import { Component } from "@angular/core";
import {Router,NavigationEnd} from '@angular/router';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "calypsofront";
  headerFooter: boolean;

  constructor(
    private router : Router
  ) {
    this.router.events
    .subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.headerFooter = (event.url !== '/connexion')
      }
    });
   }

  ngOnInit() {
  }

}
