import { Role } from "./role.model";
import { Voyage } from "./voyage.model";
import { credential } from "./credential.model";
export class Utilisateur {

  public id: number;
  public identifiantU : string;
  public nom: string;
  public prenom: string;
  public age : number ;
  public ville : string;
  public region : string;
  public pays : string;
  public voie : string;
  public hash : string;
  public telephone: string;
  public mail: string;
  public credential: credential;

  constructor(
    id: number,
    identifiantU : string,
    nom: string,
    prenom: string,
    age : number,
    ville: string,
    region : string,
    pays : string,
    voie : string,
    hash : string,
    telephone: string,
    mail: string,
    credential: credential,
  ) 
  {
    this.id = id;
    this.identifiantU = identifiantU;
    this.nom = nom;
    this.prenom = prenom;
    this.age = age;
    this.ville = ville;
    this.region = region;
    this.pays = pays
    this.voie = voie;
    this.hash = hash;
    this.telephone = telephone;
    this.mail = mail;
    this.credential = credential;
  }
}
