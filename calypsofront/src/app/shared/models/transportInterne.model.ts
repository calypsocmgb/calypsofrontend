import { Voyage } from "./voyage.model";
export class TransportInterne {
  public id: number;
  public locationDepart: string;
  public locationArrivee: string;
  public prix: number;
  public Voyage: Voyage;

  constructor(
    id: number,
    locationDepart: string,
    locationArrivee: string,
    prix: number,
    Voyage: Voyage
  ) {
    this.id = id;
    this.locationDepart = locationDepart;
    this.locationArrivee = locationArrivee;
    this.prix = prix;
    this.Voyage = Voyage;
  }
}
