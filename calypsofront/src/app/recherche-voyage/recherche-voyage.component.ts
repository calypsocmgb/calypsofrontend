import { Voyage } from 'src/app/shared/models/voyage.model';
import { FormulaireComponent } from './../homepage/header-container/formulaire/formulaire.component';
import { Pension } from './../shared/models/pension.model';
import { Hebergement } from './../shared/models/hebergement.model';
import { Theme } from './../shared/models/theme.model';
import { Component, OnInit, Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-recherche-voyage",
  templateUrl: "./recherche-voyage.component.html",
  styleUrls: ["./recherche-voyage.component.css"]
})
@Injectable()
export class RechercheVoyageComponent implements OnInit {
  Voyage: any;
  constructor(private http: HttpClient) {}

  lien: string = "";
  pathLien: string = "";

  ListPays: Array<Voyage> = [];
  ListTheme: Array<Theme> = [];
  ListHebergement: Array<Hebergement> = [];
  ListPension: Array<Pension> = [];

  ngOnInit() {
    this.lien = "Voir tous les voyages";
    this.pathLien = "resultat";
    let authen = new HttpHeaders();
    let authen2 = authen.append(
      "Authorization",
      "Basic YXBwLW5hbWU6Y2FseXBzbw=="
    );
    let authen3 = authen2.append("Access-Control-Allow-Origin", "*");
    this.getPays(authen3);
    this.getTheme(authen3);
    this.getHebergement(authen3);
    this.getPension(authen3);
  }

  //Soumission du formulaire
  onSubmit(form: NgForm) {
    console.log('salut');
    const pays = form.value['pays'];
    const themes = form.value['themes'];
    const heberg = form.value['heberg'];
    const pension = form.value['pension'];
    const prix = form.value['prix'];
    const results = this.Voyage.getListVoyages(pays, themes);
    console.log('salut');
}

  getPays(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/Voyages", {
        headers: authen
      })
      .subscribe(result => {
        this.ListPays = result;
      });
  }

  getTheme(authen: HttpHeaders): void {
    this.http
      .get<Theme[]>("http://127.0.0.1:9091/Calypso/ThemeListe", {
        headers: authen
      })
      .subscribe(result => {
        this.ListTheme = result;
      });
  }

  getHebergement(authen: HttpHeaders): void {
    this.http
      .get<Hebergement[]>("http://127.0.0.1:9091/Calypso/RechercheHebergement", {
        headers: authen
      })
      .subscribe(result => {
        this.ListHebergement = result;
      });
  }

  getPension(authen: HttpHeaders): void {
    this.http
      .get<Pension[]>("http://127.0.0.1:9091/Calypso/PensionListe", {
        headers: authen
      })
      .subscribe(result => {
        this.ListPension = result;
      });
  }
}
