import { Voyage } from "./../shared/models/voyage.model";
import { Component, OnInit, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { jsonp } from "cesium";
import { Observable } from "rxjs";
import { Options } from "selenium-webdriver/chrome";

@Component({
  selector: "app-resultat-total-voyages",
  templateUrl: "./resultat-total-voyages.component.html",
  styleUrls: ["./resultat-total-voyages.component.css"]
})
@Injectable()
export class ResultatTotalVoyagesComponent implements OnInit {
  static getVoyages(): Array<Voyage> {
    throw new Error("Method not implemented.");
  }
  Voyage: Array<Voyage> = [];
  constructor(private http: HttpClient) {}
  pathDetail: string = "";

  ngOnInit() {
    this.pathDetail = "detail";
    let authen = new HttpHeaders();
    let authen2 = authen.append(
      "Authorization",
      "Basic YXBwLW5hbWU6Y2FseXBzbw=="
    );
    let authen3 = authen2.append("Access-Control-Allow-Origin", "*");
    this.getVoyages(authen3);
  }

  getVoyages(authen: HttpHeaders): void {
    this.http
      .get<Voyage[]>("http://127.0.0.1:9091/Calypso/Voyages", {
        headers: authen
      })
      .subscribe(result => {
        this.Voyage = result;
      });
  }

  setVoyage(voyage: Voyage): void {
    this.http.post<Array<Voyage>>("URL", voyage).subscribe(result => {
      this.Voyage = result;
    });
  }
}
